import csv

def is_zcash_ticket(labels, milestone, the_label, the_milestone):
    for i in labels.split(','):
        if i == the_label:
            return(milestone==the_milestone)
    return(False)


# ['Title', 'Description', 'Issue ID', 'URL', 'State', 'Author', 'Author Username', 'Assignee', 'Assignee Username', 'Confidential', 'Locked', 'Due Date', 'Created At (UTC)', 'Updated At (UTC)', 'Closed At (UTC )', 'Milestone', 'Weight', 'Labels', 'Time Estimate', 'Time Spent']  
# Labels 17, Time Estimate 18, Milestone 15
# Labels include 'Onion Services: MUST'
# Milestone is 'Arti: Onion service support'
estimations_zcash = 0
estimations_total = 0
with open('tpo-core-arti_issues_2023-11-28.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    header = next(csv_reader)
    for row in csv_reader:
        estimations_total = estimations_total + int(row[18])
        if is_zcash_ticket(row[17], row[15], 'Onion Services: MUST', 'Arti: Onion service support'):
            print(row[18])
            estimations_zcash = estimations_zcash + int(row[18])
    print('ZCASH ', estimations_zcash)
    print('TOTAL ', estimations_total)

