Title,Description,Issue ID,URL,State,Author,Author Username,Assignee,Assignee Username,Confidential,Locked,Due Date,Created At (UTC),Updated At (UTC),Closed At (UTC),Milestone,Weight,Labels,Time Estimate,Time Spent
Vanguards in Arti,"Given that we recommend the Vanguards design for onion services, and that we're considering recommending it elsewhere, we should just build it in Rust for Arti.

This is in the %""Arti Onion service support""  milestone, since it's important for onion service security.

The version that we would want to build here is a combination of proposal 292 (mesh vanguards) and proposal 333 (vanguards lite) that would work roughly as follows.
  * There would be a configurable number of vanguard sets: either one or two.  This would correspond to a second-hop set and an optional third-hop set.  When building a circuit with vanguards, we'd choose a Guard for hop one, and a Vanguard from the 2nd-hop set for step 2, and so on.
  * We would add an extra hop as needed according to the rules of proposal 292.
  * Each vanguard set would be selected at random, and expire according to the rules from proposal 292.
  * We would not track up/down status for given vanguards, and instead simply use consensus to tell when vanguards were down.
  * Vanguards would always be persistent.
  * We would use vanguards whenever we were building circuits related to onion services, except when being a single onion service.

Rough subtasks:
 * Add a new object to GuardMgr to track vanguard sets.
 * Teach it how to sample from a netdir
 * Teach it how to expire its entries
 * Teach it to give out random members
 * Teach it how to be persistent
 * Use vanguards to build circuits according to the configured rules.",98,https://gitlab.torproject.org/tpo/core/arti/-/issues/98,Open,Nick Mathewson,nickm,"","",No,No,2024-04-30,2021-03-15 17:36:47,2024-02-21 17:54:07,,Arti: Guard discovery research,,"Backlog,Guard security: MUST,Needs Scope,Onion Services: Improved Security,Q1,Roadmap,Sponsor 119",0,0
Add vanguard configuration options,"We have a few options here:
  * have a single, global configuration option the says whether to use
    vanguards-lite, or vanguards-full. This would apply to all HS
    circuits (to both client and service circuits)
  * have separate configuration options for clients and services (the
    service vanguard config would be per-service). This would enable us
    to configure clients and services, as well as different services 
    running in the same arti instance, independently from each other 
    (I'm not sure whether this is useful though).

We might also want to add config options for overriding the
`guard-hs-l2-number`,  `guard-hs-l2-lifetime-min`, `guard-hs-l2-lifetime-max`
consensus parameters, so perhaps we need a proper `VanguardsConfig` (rather than a
simple `vanguards: auto|lite|full` option)

```toml
   [onion_services.""allium-cepa"".vanguards]
   kind = ""lite""
   l2_guard_num = 5
   # TODO: there are no consensus params for the l3 guards
   l3_guard_num = 15
   ...
```",1272,https://gitlab.torproject.org/tpo/core/arti/-/issues/1272,Open,gabi-250,gabi-250,gabi-250,gabi-250,No,No,,2024-02-13 12:35:26,2024-02-21 17:32:29,,Arti: Guard discovery research,,"Backlog,Guard security: MUST,Q1",28800,0
Implement vanguard pool persistence,"The pools will likely be stored in the state dir.

This is a prerequisite to implementing vanguards-full",1273,https://gitlab.torproject.org/tpo/core/arti/-/issues/1273,Open,gabi-250,gabi-250,gabi-250,gabi-250,No,No,,2024-02-13 12:36:12,2024-02-21 17:48:29,,Arti: Guard discovery research,,"Backlog,Guard security: MUST,Q1",86400,0
Use vanguards path selection in CircMgr::launch_hs_unmanaged.,"The path selection will be handled by `VanguardHsPathBuilder` 
(a new type similar to, or based on, the existing `ExitPathBuilder`).
See #1279

`CircMgr::launch_hs_unmanaged` will need to take an extra argument
specifying whether to use full or lite vanguards when building the circuits.

`CircMgr::launch_hs_unmanaged` will only create STUB circuits.
If full vanguards are enabled, the circuits will be extended
by one hop as needed (outside of `launch_hs_unmanaged`), whenever
 a STUB+ circuit is required (based on the `HsCircKind`).

Alternatively, `CircMgr::launch_hs_unmanaged` could
take an argument that specifies whether the circuit to launch
is STUB or STUB+. We will likely also need a corresponding 
`TargetCircUsage::HsCircBaseWithVanguards (full|lite)` for it.
Then, based on the `TargetCircUsage`, `TargetCircUsage::build_path`
can dispatch to `VanguardHsPathBuilder::pick_path` (as opposed to
the `ExitPathBuilder::pick_path` currently used for 
`TargetCircUsage::HsCircBase).

Since `launch_hs_circuits_as_needed`
preemptively populates the `HsCircPool` with circuits, it will need
to be modified too (to launch both STUB and STUB+ circuits).

Prerequisites: #1275, #1277, #1279",1274,https://gitlab.torproject.org/tpo/core/arti/-/issues/1274,Open,gabi-250,gabi-250,gabi-250,gabi-250,No,No,,2024-02-13 12:37:47,2024-02-21 17:50:36,,Arti: Guard discovery research,,"Backlog,Guard security: MUST,Q1",86400,0
Design the `VanguardMgr` and/or `VanguardPool`,TODO: split into multiple issues,1275,https://gitlab.torproject.org/tpo/core/arti/-/issues/1275,Open,gabi-250,gabi-250,gabi-250,gabi-250,No,No,,2024-02-13 12:39:27,2024-03-05 14:51:58,,Arti: Guard discovery research,,"Doing,Guard security: MUST,Q1",28800,0
Add vanguards info to the circuits in hspool::pool::Pool,"Today the pool contains a `Vec` of client circuits:
```rust
/// The collection of circuits themselves, in no particular order.
circuits: Vec<Arc<ClientCirc>>,
```

To support vanguards, we need to know for each circuit if it's
`STUB` or `STUB+`, and whether it is a vanguards-full or vanguards-lite
circuit. It should also be possible to disable vanguards altogether, so
this extra vanguards info should be optional.

This information will probably need to be returned, along with the `ClientCirc`,
from `CircMgr::launch_hs_unmanaged`. This could be included alongside
`ClientCirc`, or as a field within it (gated behind the `hs-common`
feature), depending on what we decide in #1274.",1276,https://gitlab.torproject.org/tpo/core/arti/-/issues/1276,Open,gabi-250,gabi-250,gabi-250,gabi-250,No,No,,2024-02-13 12:42:31,2024-02-21 17:53:43,,Arti: Guard discovery research,,"Backlog,Guard security: MUST,Q1",86400,0
Make VanguardMgr accessible to CircMgr,"`CircMgr::launch_hs_unmanaged` will need to be able to launch circuits that use vanguards, so `CircMgr` will need a handle to `VanguardMgr`.

Depends on #1275",1277,https://gitlab.torproject.org/tpo/core/arti/-/issues/1277,Open,gabi-250,gabi-250,gabi-250,gabi-250,No,No,,2024-02-13 15:07:37,2024-02-21 17:43:26,,Arti: Guard discovery research,,"Backlog,Guard security: MUST,Q1",28800,0
Implement VanguardHsPathBuilder,Or modify the existing `ExitPathBuilder` to provide path selection using vanguards.,1279,https://gitlab.torproject.org/tpo/core/arti/-/issues/1279,Open,gabi-250,gabi-250,gabi-250,gabi-250,No,No,,2024-02-13 15:25:23,2024-02-21 17:43:51,,Arti: Guard discovery research,,"Backlog,Guard security: MUST,Q1",86400,0
